# -*- coding: utf-8 -*-
"""
Created on Tue Feb  8 14:04:09 2022

@author: hensir
"""
import torch
import time
from torch import nn
#from torch.autograd import gradcheck
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import numpy as np
from datahandler import *
import math
from tqdm import tqdm, trange
#from time import time
import time
from sklearn.metrics import roc_auc_score
import json
import argparse
import os
import pickle as pkl

from glob import glob

from params import CFG_DIR, FEAT_LIST
from train import train
from datahandler import *
from RNN import RNN
from GRU import GRU
from LSTM import LSTM
import socket


SERVER=False
if socket.gethostname() == "c-cb47.ad.cmm.se":
    SERVER = True

def getParams():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", type=str, help="Input config")
    args = parser.parse_args()
    
    if SERVER:
        outfname = os.path.join("res",
                                os.path.basename(os.path.dirname(args.i)),
                                os.path.basename(args.i).replace(".json", ".pkl"))
        cfg = json.load(open(args.i))
    else:
        cfg = json.load(open(CFG_DIR))
    data_cfgs = cfg["data"]
    model_cfgs = cfg["model"]
    train_cfgs = cfg["train"]
    print("data_cfgs:\n", data_cfgs)
    print("model_cfgs:\n", model_cfgs)
    print("train_cfgs:\n", train_cfgs)
    
    """
    make exp_dir=conv2d singularity exec --nv ../../mycontainer.sif python3 factorialHMM.py -i configs/conv2d/10.json
    """
    return data_cfgs, model_cfgs, train_cfgs, cfg

def synthesize(model, x, y, title_label, negative_data=False):
    """
    N = Batch size
    L = Sequence length
    D = 2 if bidirectional, 1 otherwise
    H_in = input_size
    H_out = hidden_size
    """
    plot_list = []
    plot_tensor = None
 
    out = model.forward(x)
    plot_tensor = out.detach()
    y = y[:,:,-1]
    
    # Create plotable list:
    label_list = []
    for i in range(plot_tensor.shape[0]):
        try:
            plot_list += plot_tensor[i].cpu().tolist()
            label_list += y[i].tolist()
        except TypeError:
            plot_list += [plot_tensor[i].cpu().tolist()]
            label_list += [y[0][i].tolist()]

    if negative_data:
        prediction_vals = pd.DataFrame(data={"prediction":plot_list, "label":label_list})
        return prediction_vals
    else:
        """
        time_to_event_h = time_to_event_h.astype(int)
        time_to_event_h = list(map(int,np.round(time_to_event_h.to_numpy()).tolist()))
        if len(plot_list)>len(time_to_event_h):
            n_time_to_event_h = len(time_to_event_h)
            context_padding = np.zeros(len(plot_list)-n_time_to_event_h).tolist()
            context_padding = list(map(int,context_padding))
            print(context_padding)
            time_to_event_h += context_padding
            time_to_event_h = np.array(time_to_event_h)
        else:
            time_to_event_h = time_to_event_h.to_numpy()
        #"""  
        fig, ax = plt.subplots(1,1)
        ax.plot(plot_list, "g", label="Prediction")
        ax.plot(label_list, "b", label="Label")
        ax.legend()
        ax.set_title(title_label)

        prediction_vals = pd.DataFrame(data={"prediction":plot_list, "label":label_list})
        return prediction_vals, fig

def getModel(model, input_dim, hidden_dim, layer_dim, output_dim, device):
    if model == "RNN":
        return RNN(input_dim, hidden_dim, layer_dim, output_dim, device)
    elif model == "GRU":
        return GRU(input_dim, hidden_dim, layer_dim, output_dim, device)
    elif model == "LSTM":
        return LSTM(input_dim, hidden_dim, layer_dim, output_dim, device)
    else: raise Exception("No model specified")

def trainingPrep(train_folds, test_folds, SEQUENCE_LENGTH, negative_data=None):
    #print("trainingPrep")
    #"""
    train_set = train_folds
    test_set = test_folds
    
    #print("- train_set:", train_set.shape)
    #print("- test_set:", test_set.shape)
    #print(train_set.shape[0] + test_set.shape[0])
    
    # Process train_set:
    train_set, gap_value = replaceGapsWithNaN(train_set, print_minmax=False)
    #gap_value = 0.0
    #train_set = fillNaNs(train_set, gap_value)
    train_set, scaler = standardizeData(train_set)
    train_set = fillNaNs(train_set, gap_value) # Add gap_value after standardization
    
    # Process test_set:
    test_set, _ = replaceGapsWithNaN(test_set, print_minmax=False)
    #test_set = fillNaNs(test_set, gap_value)
    test_set = scaleTestSet(test_set, scaler)
    test_set = fillNaNs(test_set, gap_value)  # Add gap_value after standardization
    
    # Sequencify train_set:
    batched_train_data, _ = batchify(train_set)
    sequenced_train_batches = sequencify(batched_train_data, SEQUENCE_LENGTH)
    
    train_set = []
    for patient in sequenced_train_batches.keys():
        train_set.append(sequenced_train_batches[patient])
    
    # Sequencify test_set:
    batched_test_data, _ = batchify(test_set)
    sequenced_test_batches = sequencify(batched_test_data, SEQUENCE_LENGTH)
    
    test_set = []
    for patient in sequenced_test_batches.keys():
        test_set.append(sequenced_test_batches[patient])
    #raise Exception("PAUSE")
    #"""
    
    negative_test_set = negative_data
    # Process negative patients
    if negative_data is not None:
        negative_test_set, _ = replaceGapsWithNaN(negative_test_set, print_minmax=False)
        negative_test_set = scaleTestSet(negative_test_set, scaler)
        negative_test_set = fillNaNs(negative_test_set, gap_value)  # Add gap_value after standardization
        
        batched_negative_test_data, _ = batchify(negative_test_set)
        sequenced_negative_test_batches = sequencify(batched_negative_test_data, SEQUENCE_LENGTH)
        
        negative_test_set = []
        for patient in sequenced_negative_test_batches.keys():
            negative_test_set.append(sequenced_negative_test_batches[patient])
        return train_set, test_set, negative_test_set
    else:
        return train_set, test_set

def main(save=False):
    # --- Try GPU --- #
    device = "cpu"
    if torch.cuda.is_available():
        device = "cuda"
        time.sleep(np.random.rand()*10)
        device = get_freer_gpu()
    devicePrint(device)
    # --- Try GPU --- #
    
    # Get params:
    data_cfgs, model_cfgs, train_cfgs, cfgs = getParams()
    
    # PARAMS #
    if not SERVER:
        SEQUENCE_LENGTH = data_cfgs["SEQUENCE_LENGTH"][0]
        MODEL = model_cfgs["MODEL"][0]
        HIDDEN_DIM = model_cfgs["HIDDEN_DIM"][0]
        LAYER_DIM = model_cfgs["LAYER_DIM"][0]
        LR = train_cfgs["LR"][0]
        EPOCHS = train_cfgs["EPOCHS"][0]
        LOSS = train_cfgs["LOSS"][0]
        FOLDS = train_cfgs["FOLDS"][0]
    else:
        SEQUENCE_LENGTH = data_cfgs["SEQUENCE_LENGTH"]
        MODEL = model_cfgs["MODEL"]
        HIDDEN_DIM = model_cfgs["HIDDEN_DIM"]
        LAYER_DIM = model_cfgs["LAYER_DIM"]
        LR = train_cfgs["LR"]
        EPOCHS = train_cfgs["EPOCHS"]
        LOSS = train_cfgs["LOSS"]
        FOLDS = train_cfgs["FOLDS"]
    # PARAMS #
    
    # --- Preprocessing --- #
    train_folds, test_folds, negative_data, class_weights = initial_prerpocessing_feat(folds=FOLDS)
    # --- Preprocessing --- #
    """
    Note: Data contains currently only patients with sepsis!
    """ 
    input_dim = len(FEAT_LIST)
    hidden_dim = HIDDEN_DIM # Number of features in the hidden state (window size)
    layer_dim = LAYER_DIM # Nr layers
    output_dim = 1

    # Get model:
    #model_name = "RNN"
    model_name = MODEL
    model = getModel(model_name, input_dim, HIDDEN_DIM, LAYER_DIM, output_dim, device)
    
    print("Training start:")
    folds = FOLDS # len(test_folds)
    RESULTS = {"avg_training_time": [], 
               "final_training_losses": [],
               "final_validation_losses": [],
               "training_loss_list_per_fold": [],
               "validation_loss_list_per_fold":[],
               "positive_prediction_values":[],
               "negative_prediction_values":[],
               "unpadded_context_time_to_event_h":[]
               }
    #loss_figs = []
    #prediction_figs = []
    figs = []
    for i in range(folds):
        positive_prediction_values = []
        negative_prediction_values = []
        
        print("--- FOLD: " + str(i+1) + " ---")
        train_set, test_set, negative_set = trainingPrep(train_folds[i], test_folds[i], SEQUENCE_LENGTH, negative_data=negative_data)
                
        model, results = train(train_set, model, device, LR, EPOCHS, LOSS, test_set, class_weights)
        
        name = "Model_" + str(MODEL) + "_Fold_" + str(i+1) + "," + str(folds) + "_Loss_" + str(LOSS) + "_Layers_" + str(LAYER_DIM) + "_Epocs_" + str(EPOCHS) + "_Seqlen_" + str(SEQUENCE_LENGTH)
        fig, ax = plt.subplots(1,1)
        ax.plot(results["loss_list"], "b", label="Training loss")
        ax.plot(results["val_loss_list"], "r", label="Validation loss")
        title_label = "Model: " + str(MODEL) + " Fold: " + str(i+1) + "/" + str(folds) + " Loss: " + str(LOSS)
        ax.set_title(title_label)
        ax.legend()
        figs.append(fig)
        
        #if not SERVER:
            #fig.savefig("plots/{}.pdf".format(name))
        for label_id in range(len(test_set)):
            x = test_set[label_id][0].to(device)
            y = test_set[label_id][1]
            test_time_to_event_h = test_folds[label_id]["context__time_to_event_h"]
            values, fig = synthesize(model, x, y, title_label+" ID: " + str(label_id+1), negative_data=False)
            positive_prediction_values.append(values)
            figs.append(fig)
            
        for neg_label_id in range(len(negative_set)):
            x = negative_set[neg_label_id][0].to(device)
            y = negative_set[neg_label_id][1]
            values = synthesize(model, x, y, title_label+" ID: " + str(neg_label_id+1), negative_data=True)
            negative_prediction_values.append(values)
            
        #if save:
            #torch.save(model,"models/"+name+".pkl")
        
        RESULTS["unpadded_context_time_to_event_h"].append(test_time_to_event_h)
        RESULTS["avg_training_time"].append(results["training_time"])
        RESULTS["final_training_losses"].append(results["final_training_loss"])
        RESULTS["final_validation_losses"].append(results["final_validation_loss"])
        RESULTS["training_loss_list_per_fold"].append(results["loss_list"])
        RESULTS["validation_loss_list_per_fold"].append(results["val_loss_list"])
        RESULTS["positive_prediction_values"].append(positive_prediction_values)
        RESULTS["negative_prediction_values"].append(negative_prediction_values)
    
    RESULTS["figs"] = [pkl.dumps(f) for f in figs]
    #RESULTS["loss_figs"] = [pkl.dumps(f) for f in loss_figs]
    #RESULTS["prediction_figs"] = [pkl.dumps(f) for f in prediction_figs]
    #RESULTS["avg_final_training_loss"] = sum(RESULTS["avg_final_training_loss"])/len(RESULTS["avg_final_training_loss"])
    #RESULTS["avg_final_validation_loss"] = sum(RESULTS["avg_final_validation_loss"])/len(RESULTS["avg_final_validation_loss"])
    
    pkl_file_name = "root/Model_{}_Sequence_{}_Hidden_{}_Layer_{}_Loss_{}_Folds_{}".format(MODEL, SEQUENCE_LENGTH, HIDDEN_DIM, LAYER_DIM, LOSS, FOLDS)
    pkl.dump([cfgs, RESULTS], open(pkl_file_name + ".pkl", "wb+"))

main(save=False)